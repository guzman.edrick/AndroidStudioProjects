package com.example.excel.myapplication;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.view.View;
import android.widget.Button;

/**
 * Created by EXCEL on 5/25/2016.
 */
public class act10_Notif extends Activity {

    Button btNotif;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act10_notif);

        btNotif = (Button) findViewById(R.id.btNotif);
    }

    public void Notify(View view){
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this);
        mBuilder.setSmallIcon(R.drawable.ic_launcher);
        mBuilder.setContentTitle("This is Important");
        mBuilder.setContentText("You have been notified");
        mBuilder.setAutoCancel(true);

        Intent intent = new Intent("com.example.excel.myapplication.act3_ListView");
        TaskStackBuilder stackbuilder = TaskStackBuilder.create(this);
        stackbuilder.addParentStack(act3_ListView.class);
        stackbuilder.addNextIntent(intent);


        PendingIntent pendingintent = stackbuilder.getPendingIntent(0,PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(pendingintent);

        NotificationManager nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        nm.notify(0,mBuilder.build());
    }


}
