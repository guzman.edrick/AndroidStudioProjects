package com.example.excel.myapplication;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;

/**
 * Created by EXCEL on 5/20/2016.
 */
public class act7_ContactDetails extends Activity {

    EditText edName,edEmail;
    dbconnect dbHandler;
    String name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act7_contactdetails);
        init();
        getContactDetails();
    }

    public void init(){
        edName = (EditText) findViewById(R.id.edName);
        edEmail = (EditText) findViewById(R.id.edEmail);
        dbHandler = new dbconnect(getBaseContext());
    }

    public void getContactDetails(){

        dbHandler.open();

            Intent intent = getIntent();
            name = intent.getStringExtra("paramName");

            Cursor curContent = dbHandler.SearchContacts(name);
            curContent.moveToFirst();

            edName.setText(curContent.getString(1));
            edEmail.setText(curContent.getString(0));

        dbHandler.close();
    }


}
