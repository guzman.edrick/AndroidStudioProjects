package com.example.excel.myapplication;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Map;


import static android.widget.Toast.LENGTH_LONG;

/**
 * Created by EXCEL on 5/24/2016.
 */
public class act9_ExternalStorage extends Activity {

    EditText edFile;
    Button btSave, btRead;
    TextView tvRead;
    String msg = "", Filename;


    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act9_externalstorage);

        init();
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    private void init() {
        edFile = (EditText) findViewById(R.id.edFile);
        btSave = (Button) findViewById(R.id.btSave);
        btRead = (Button) findViewById(R.id.btRead);
        tvRead = (TextView) findViewById(R.id.tvFile);

    }

    public void SaveFile(View view) {

        if (checkState()) {

            try {
                File root = Environment.getExternalStorageDirectory();
                //File dir = new File(root.getAbsolutePath() + "/mnt/MyAppFile");
                File dir = new File(root + "/MyAppFile");
                if (!dir.exists()) {
                    dir.mkdir();
                }

                File file = new File(dir, "SampleExternalFile.txt");
                String msg = edFile.getText().toString();

                FileOutputStream fileoutput = new FileOutputStream(file);
                fileoutput.write(msg.getBytes());
                fileoutput.close();
                Toast.makeText(this, "File Saved", LENGTH_LONG).show();

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {

            }
        } else {
            Toast.makeText(this, "No External Memory", LENGTH_LONG).show();
        }
    }

    public void ReadFile(View view) {
        try {
            File root = Environment.getExternalStorageDirectory();
            File dir = new File(root.getAbsolutePath() + "/mnt/MyAppFile");

            File file = new File(dir, "SampleExternalFile.txt");


            FileInputStream fileinput= new FileInputStream(file);
            InputStreamReader inputstream = new InputStreamReader(fileinput);
            BufferedReader bufferedreader = new BufferedReader(inputstream);

            StringBuffer stringbuffer = new StringBuffer();

            while((msg=bufferedreader.readLine())!= null){
                stringbuffer.append(msg+"\n");
            }
            edFile.setText(stringbuffer);
            fileinput.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {


        }
    }

    public Boolean checkState() {
        String State;

        Map<String, File> externalLocations = ExternalStorage.getAllStorageLocations();
        File sdCard = externalLocations.get(ExternalStorage.SD_CARD);
        File externalSdCard = externalLocations.get(ExternalStorage.EXTERNAL_SD_CARD);

        State = sdCard.getAbsolutePath();

        Toast.makeText(this, Environment.getExternalStorageDirectory().toString(), LENGTH_LONG).show();
        Toast.makeText(this, State, LENGTH_LONG).show();



        return false;
        /*
        if(isExternalStorageEmulated()){
            File dir = new File("/mnt/sdcard/");
            if (!dir.exists()) {
                return false;
            }
            else{
                return true;
            }
        }
        else
        {
            State = Environment.getExternalStorageState();
            if(Environment.MEDIA_MOUNTED.equals(State))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        */
        /*
        Toast.makeText(this, Environment.getExternalStorageDirectory().toString(), LENGTH_LONG).show();
        return false; */
        /*
        if(Environment.MEDIA_MOUNTED.equals(State))
        {
            return true;
        }
        else
        {
           return false;
        }*/

    }

}
