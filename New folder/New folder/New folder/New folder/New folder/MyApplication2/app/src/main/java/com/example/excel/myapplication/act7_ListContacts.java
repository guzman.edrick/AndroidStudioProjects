package com.example.excel.myapplication;

import android.app.ListActivity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by EXCEL on 5/20/2016.
 */
public class act7_ListContacts extends ListActivity {
    dbconnect dbHandler;
    List<String> result = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act7_listcontacts);

        getContacts();

        setListAdapter(new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,result));

    }

    public void getContacts(){
        dbHandler = new dbconnect(getBaseContext());
        dbHandler.open();
        Cursor content = dbHandler.showContacts();

        content.moveToFirst();

        do{
            String name = content.getString(0);
            result.add(name);
        }
        while(content.moveToNext());

        dbHandler.close();
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);

        String name = ""+l.getItemAtPosition(position);
        Intent intent = new Intent("com.example.excel.myapplication.act7_ContactDetails");
        intent.putExtra("paramName",name);
        startActivity(intent);
      //  Toast.makeText(this,name,Toast.LENGTH_SHORT).show();

    }
}
