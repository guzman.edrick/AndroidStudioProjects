package com.example.excel.myapplication;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

/**
 * Created by EXCEL on 5/19/2016.
 */
public class dbconnect {

    public static final String DB_NAME="myDatabase";
    public static final String TABLE_NAME="myTable";
    public static final String NAME="name";
    public static final String EMAIL="email";
    public static final int DB_VERSION=1;

    public static final String DB_CREATE="CREATE TABLE myTable (name text not null,email text not null)";

    DatabaseHelper dbHelper;
    Context ctx;
    SQLiteDatabase db;

    public dbconnect(Context ctx){
        this.ctx=ctx;
        dbHelper=new DatabaseHelper(ctx);

    }

    private static class DatabaseHelper extends SQLiteOpenHelper{

        public DatabaseHelper(Context ctx){
            super(ctx, DB_NAME, null,DB_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(DB_CREATE);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL("DROP TABLE on Exists myTable");
            onCreate(db);
        }
    }

    public dbconnect open(){
        db=dbHelper.getWritableDatabase();
        return this;
    }

    public void close(){
        dbHelper.close();
    }

    public long insertContacts(String name, String email){
        ContentValues data = new ContentValues();
        data.put(NAME,name);
        data.put(EMAIL,email);

        return db.insertOrThrow(TABLE_NAME,null,data);

    }

    public Cursor showContacts(){

        String strSQLQuery = "SELECT * FROM myTable";
        return db.rawQuery(strSQLQuery,null);
    }

    public Cursor SearchContacts(String paramSearch){

        String strSQLQuery = "SELECT email,name FROM myTable WHERE name = '"+paramSearch+"'";
        return db.rawQuery(strSQLQuery,null);
    }


    public void updateContacts(String paramWhere, String paramUpdate){

        String strSQLQuery = "UPDATE myTable set email = '"+paramUpdate+"' WHERE name = '"+paramWhere+"'";
        db.execSQL(strSQLQuery);

    }


}
