package com.example.excel.myapplication;

import android.app.ListActivity;
import android.content.Context;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

/**
 * Created by EXCEL on 5/16/2016.
 */
public class act4_CustomListView extends ListActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act3_listview);

      //  setListAdapter(new MyAdapter(this,android.R.layout.simple_list_item_1,R.id.tvCustomView, getResources().getStringArray(R.array.listitems)));
        setListAdapter(
                new MyAdapter(this,android.R.layout.simple_list_item_1,
                        R.id.tvCustomView,
                        getResources().getStringArray(R.array.listitems)
                )
        );
    }

    private class MyAdapter extends ArrayAdapter<String>{
        public MyAdapter(Context context, int resource, int textViewResourceId, String[] strings) {
            super(context, resource, textViewResourceId, strings);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View row=inflater.inflate(R.layout.act4_customview,parent,false);

            ImageView iv=(ImageView) row.findViewById(R.id.imgCustomView);
            TextView tv=(TextView) row.findViewById(R.id.tvCustomView);
            String[] items=getResources().getStringArray(R.array.listitems);

            tv.setText(items[position]);


            if(items[position].equals("Map")) {
                iv.setImageResource(R.drawable.maps);

            }

            if(items[position].equals("Calendar")) {
                iv.setImageResource(R.drawable.calendar);
            }

            if(items[position].equals("Messages")) {
                iv.setImageResource(R.drawable.messages);
            }

            if(items[position].equals("Recorder")) {
                iv.setImageResource(R.drawable.recorder);
            }
            return row;
           // return super.getView(position, convertView, parent);
        }
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);

        switch(position){
            case 0:
                Toast.makeText(this,"You Select "+ position,Toast.LENGTH_SHORT).show();
                break;
            case 1:
                Toast.makeText(this,"You Select "+ position,Toast.LENGTH_SHORT).show();
                break;
            case 2:
                Toast.makeText(this,"You Select "+ position,Toast.LENGTH_SHORT).show();
                break;
            case 3:
                Toast.makeText(this,"You Select "+ position,Toast.LENGTH_SHORT).show();
                break;
        }

    }
}
