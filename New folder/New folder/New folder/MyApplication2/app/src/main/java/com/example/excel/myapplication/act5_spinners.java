package com.example.excel.myapplication;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.Spinner;
import android.widget.Toast;

/**
 * Created by EXCEL on 5/16/2016.
 */
public class act5_spinners extends Activity implements AdapterView.OnItemSelectedListener {
    Spinner spin1;
    CheckBox check1;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.act5_spinners);
        spin1 = (Spinner) findViewById(R.id.idspinner);
        spin1.setOnItemSelectedListener(this);

        check1 = (CheckBox) findViewById(R.id.checkStatus);


        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,R.array.listitems,android.R.layout.simple_spinner_dropdown_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spin1.setAdapter(adapter);
    }

    public void checkStat(View view){
        if(check1.isChecked()==true){
            Toast.makeText(this,"On",Toast.LENGTH_SHORT).show();
        }else{
            Toast.makeText(this,"Off",Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


        if(position==0){
            Toast.makeText(this,"Selected",Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}


