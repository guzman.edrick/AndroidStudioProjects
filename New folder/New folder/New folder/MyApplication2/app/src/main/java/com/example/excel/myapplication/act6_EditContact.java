package com.example.excel.myapplication;

import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by EXCEL on 5/19/2016.
 */
public class act6_EditContact extends Activity{

    Button btSearch,btUpdate;
    TextView tvEmail;
    EditText edSearch,edEmail;

    dbconnect dbHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.act6_editcontact);

        init();
        dbHandler = new dbconnect(getBaseContext());
    }

    public void init(){
        btSearch = (Button) findViewById(R.id.btSearch);
        btUpdate = (Button) findViewById(R.id.btUpdate);

        tvEmail = (TextView) findViewById(R.id.tvEmail);

        edSearch = (EditText) findViewById(R.id.edSearch);
        edEmail = (EditText) findViewById(R.id.edEmail);
    }


    public void SearchName(View view){


        String name,email;

        name = edSearch.getText().toString();

        dbHandler.open();
        Cursor content = dbHandler.SearchContacts(name);

        if(content.getCount()>0) {
            content.moveToFirst();
            email = content.getString(0);
            // tvEmail.setVisibility(view.VISIBLE);
            tvEmail.setText(email);
        }

        dbHandler.close();
    }

    public void UpdateEmail(View view){

        String name,email;
        name = edSearch.getText().toString().trim();
        email = edEmail.getText().toString().trim();

        dbHandler.open();
        dbHandler.updateContacts(name,email);

        Toast.makeText(this,"Record Saved",Toast.LENGTH_LONG).show();

        dbHandler.close();
    }
}
