package com.example.excel.myapplication;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.TaskStackBuilder;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.preference.DialogPreference;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

/**
 * Created by EXCEL on 5/15/2016.
 */
public class Sample extends Activity {

    EditText edName;
    Button btOk, btReset,btExit;
    TextView tvGreet;

    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.sample);
        Initialize();

        btOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String name=edName.getText().toString();
                tvGreet.setText("Hi "+name);

            }
        });

        btReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                edName.setText("");
                tvGreet.setText("");

            }
        });

    }

    public void Initialize() {

        edName = (EditText) findViewById(R.id.EdName);
        btOk = (Button) findViewById(R.id.btOk);
        btReset = (Button) findViewById(R.id.btReset);
        btExit = (Button) findViewById(R.id.btExit);
        tvGreet = (TextView) findViewById(R.id.tvGreet);

    }

    public void exit(View view){
        /*
        Toast.makeText(Sample.this,"Will now exit the application",Toast.LENGTH_SHORT).show();
        */

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle("Header");
        alertDialogBuilder.setMessage("Are you sure you want to exit?");

        alertDialogBuilder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                Toast.makeText(Sample.this,"GoodBye",Toast.LENGTH_LONG).show();

            }
        });

        alertDialogBuilder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                Toast.makeText(Sample.this,"Cancelled",Toast.LENGTH_LONG).show();

            }
        });

        AlertDialog alert = alertDialogBuilder.create();
        alert.show();


    }


}
