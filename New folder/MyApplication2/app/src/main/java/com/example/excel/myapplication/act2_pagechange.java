package com.example.excel.myapplication;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

/**
 * Created by EXCEL on 5/15/2016.
 */
public class act2_pagechange extends Activity{

    Button btGo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.act2_pagechange);
        Initialize();

        Thread timer=new Thread(){
            public void run() {
                try{
                    sleep(5000);

                }
                catch (InterruptedException e){
                    e.printStackTrace();

                }
                finally {

                    Intent openWindow= new Intent("com.example.excel.myapplication.Sample");
                    startActivity(openWindow);
                }

            }
        };

        timer.start();
    }

    public void Initialize(){
        btGo = (Button) findViewById(R.id.btGo);

    }

    public void openActivity_1(View view){

        /*
        Intent openWindow= new Intent("com.example.excel.myapplication.Sample");
        startActivity(openWindow);
        */


    }

}
