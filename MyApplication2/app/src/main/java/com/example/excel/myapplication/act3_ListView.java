package com.example.excel.myapplication;

import android.app.ListActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.Toast;

/**
 * Created by EXCEL on 5/16/2016.
 */
public class act3_ListView extends ListActivity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act3_listview);

        setListAdapter(new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, getResources().getStringArray(R.array.listitems)));

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater Inflater = getMenuInflater();
        Inflater.inflate(R.menu.main_menu,menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch(item.getItemId()){
            case R.id.aboutus:
                Toast.makeText(this,"You Selected About Us",Toast.LENGTH_SHORT).show();
                break;
            case R.id.contactus:
                Toast.makeText(this,"You Selected Contact Us",Toast.LENGTH_SHORT).show();
                break;

            case R.id.refresh:
                Toast.makeText(this,"You Selected Refresh",Toast.LENGTH_SHORT).show();
                break;

            case R.id.neverid:
                Toast.makeText(this,"You Selected Never",Toast.LENGTH_SHORT).show();
                break;
        }

        return super.onOptionsItemSelected(item);
    }
}
