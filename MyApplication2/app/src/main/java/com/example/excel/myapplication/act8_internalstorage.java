package com.example.excel.myapplication;

import android.app.Activity;
import android.os.Bundle;
import android.test.suitebuilder.TestMethod;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by EXCEL on 5/20/2016.
 */
public class act8_internalstorage extends Activity {

    EditText edFile;
    Button btSave,btRead;
    TextView tvRead;
    String msg ="",Filename;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act8_internalstorage);

        init();
    }

    private void init(){
        edFile = (EditText) findViewById(R.id.edFile);
        btSave = (Button) findViewById(R.id.btSave);
        btRead = (Button) findViewById(R.id.btRead);
        tvRead = (TextView) findViewById(R.id.tvFile);

    }

    public void SaveFile(View view){
        msg = edFile.getText().toString();
        Filename = "SampleFile";

                try
                {
                    FileOutputStream fileoutput= openFileOutput(Filename,MODE_PRIVATE);
                    fileoutput.write(msg.getBytes());
                    fileoutput.close();
                    Toast.makeText(this,"File Saved",Toast.LENGTH_LONG).show();

                }
                catch(FileNotFoundException e)
                {
                    e.printStackTrace();
                }
                catch(IOException e)
                {
                    e.printStackTrace();
                }
                finally
                {

                }
    }

    public void ReadFile(View view){

        Filename = "SampleFile";

        try
        {
            FileInputStream fileinput= openFileInput(Filename);
            InputStreamReader inputstream = new InputStreamReader(fileinput);
            BufferedReader bufferedreader = new BufferedReader(inputstream);

            StringBuffer stringbuffer = new StringBuffer();

            while((msg=bufferedreader.readLine())!= null){
                stringbuffer.append(msg+"\n");
            }
            tvRead.setText(stringbuffer);
            fileinput.close();


        }
        catch(FileNotFoundException e)
        {
            e.printStackTrace();
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
        finally
        {

        }
    }
}
