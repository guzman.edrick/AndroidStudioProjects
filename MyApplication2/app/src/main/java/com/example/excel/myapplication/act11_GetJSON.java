package com.example.excel.myapplication;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by EXCEL on 5/25/2016.
 */
public class act11_GetJSON  extends Activity{

    Button btView;
    TextView tvView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act11_getjson);

        btView = (Button) findViewById(R.id.btView);
        tvView = (TextView) findViewById(R.id.tvView);

    }

    public void ViewJSON(View view){

        new BackGroundTask().execute();
    }

    public class BackGroundTask extends AsyncTask<Void, Void, String>{

        @Override
        protected String doInBackground(Void... params) {
            String JSONurl = "http://192.168.1.3/20160525/index.php";
            String JSONstring;

            try{

                URL url = new URL(JSONurl);
                HttpURLConnection urlConn = (HttpURLConnection) url.openConnection();
                InputStream is = urlConn.getInputStream();
                BufferedReader br =  new BufferedReader(new InputStreamReader(is));
                StringBuilder stringbuilder = new StringBuilder();

                while((JSONstring=br.readLine())!=null){
                    stringbuilder.append(JSONstring+"\n");
                }

                br.close();
                is.close();
                urlConn.disconnect();
                return stringbuilder.toString();

            }catch(MalformedURLException e){
                e.printStackTrace();
            }catch(IOException e){
                e.printStackTrace();
            }
            return "test";
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            tvView.setText(s);

        }
    }

}
