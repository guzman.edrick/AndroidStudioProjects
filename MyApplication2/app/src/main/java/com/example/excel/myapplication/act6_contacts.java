package com.example.excel.myapplication;

import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by EXCEL on 5/19/2016.
 */
public class act6_contacts extends Activity {

    Button btSave,btLoad,btNext,btPrev;
    TextView tvName,tvEmail;
    EditText edName,edEmail;
    int position;

    dbconnect dbHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act6_contacts);

        init();
        dbHandler = new dbconnect(getBaseContext());
    }

    public void init() {

        btSave = (Button) findViewById(R.id.btSave);
        btLoad = (Button) findViewById(R.id.btLoad);
        btNext = (Button) findViewById(R.id.btNext);
        btPrev = (Button) findViewById(R.id.btPrev);

        tvName = (TextView) findViewById(R.id.tvName);
        tvEmail = (TextView) findViewById(R.id.tvEmail);

        edName = (EditText) findViewById(R.id.edName);
        edEmail = (EditText) findViewById(R.id.edEmail);
        position = -1;

    }

    public void SaveContact(View view){

        dbHandler.open();

        String name = edName.getText().toString();
        String email = edEmail.getText().toString();

        long data = dbHandler.insertContacts(name,email);

        Toast.makeText(this,"Records Saved",Toast.LENGTH_LONG).show();

        dbHandler.close();
    }

    public void LoadContact(View view){

        String name,email;

        dbHandler.open();

            Cursor content = dbHandler.showContacts();
            content.moveToPosition(0);
            name = content.getString(0);
            email = content.getString(1);

        tvName.setVisibility(view.VISIBLE);
        tvName.setText(name);

        tvEmail.setVisibility(view.VISIBLE);
        tvEmail.setText(email);


        dbHandler.close();
    }

    public void NextRecord(View view){

        dbHandler.open();
        Cursor content = dbHandler.showContacts();
        String name,email;

        position++;
        if(position<=content.getCount()-1){

            content.moveToPosition(position);
            name = content.getString(0);
            email = content.getString(1);

            tvName.setVisibility(view.VISIBLE);
            tvName.setText(name);

            tvEmail.setVisibility(view.VISIBLE);
            tvEmail.setText(email);


        }else{
            position -= 1;
            Toast.makeText(this,"End of file",Toast.LENGTH_LONG).show();
        }

        dbHandler.close();
    }

    public void PrevRecord(View view){
        dbHandler.open();
        Cursor content = dbHandler.showContacts();
        String name,email;

        position -= 1;
        if(position >= 0){
            Toast.makeText(this,""+position,Toast.LENGTH_LONG).show();
            content.moveToPosition(position);
            name = content.getString(0);
            email = content.getString(1);

            tvName.setVisibility(view.VISIBLE);
            tvName.setText(name);

            tvEmail.setVisibility(view.VISIBLE);
            tvEmail.setText(email);



        }else{
            position++;
            Toast.makeText(this,"End of file",Toast.LENGTH_LONG).show();
        }

        dbHandler.close();



    }
}
