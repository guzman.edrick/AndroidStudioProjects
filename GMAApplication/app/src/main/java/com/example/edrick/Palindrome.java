package com.example.edrick;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.edrick.gmaapplication.MainActivity;
import com.example.model.palindromeActivityMethods;

import com.example.edrick.gmaapplication.R;

public class Palindrome extends AppCompatActivity {
    private static Button palindrome;
    private static Button back;



    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_palindrome);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        OnClickButtonListener();
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    public void OnButtonClick(View v)
    {
        EditText e1 = (EditText)findViewById(R.id.word);
        TextView answer = (TextView)findViewById(R.id.answer);
        String word = e1.getText().toString();
        String answers = palindromeActivityMethods.checkIfPalindrome(word);
        answer.setText(answers);

    }

    public void OnClickButtonListener(){
        back = (Button)findViewById(R.id.back);
        back.setOnClickListener(
                new View.OnClickListener(){
                    @Override
                    public void onClick(View v){
                        Intent intent = new Intent(Palindrome.this, MainActivity.class);
                        startActivity(intent);
                    }
                }
        );
    }
}
