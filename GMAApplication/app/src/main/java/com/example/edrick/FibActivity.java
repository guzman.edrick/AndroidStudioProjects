package com.example.edrick;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.edrick.gmaapplication.MainActivity;
import com.example.model.fibActivityMethods;

import com.example.edrick.gmaapplication.R;

public class FibActivity extends AppCompatActivity {
    private static Button fib;
    private static Button factorial;
    private static TextView answer;
    private static Button back;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fib);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        OnClickButtonListener();
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    public void OnButtonClick(View v){

        EditText e1 = (EditText)findViewById(R.id.input);
        TextView t1 = (TextView)findViewById(R.id.answer);
        int input = Integer.parseInt(e1.getText().toString());
        int fibonacci = fibActivityMethods.fibonacci(input);
        t1.setText("The number in the  (" + input + ")th in the fib sequence is " + fibonacci);
    }

    public void OnButtonClick2(View v){

        EditText e1 = (EditText)findViewById(R.id.input);
        TextView t1 = (TextView)findViewById(R.id.answer);
        int input = Integer.parseInt(e1.getText().toString());
        int factorial = fibActivityMethods.factorial(input);
        t1.setText("The factorial of " + input + " is " + factorial);
    }

    public void OnClickButtonListener(){
        back = (Button)findViewById(R.id.back);
        back.setOnClickListener(
                new View.OnClickListener(){
                    @Override
                    public void onClick(View v){
                        Intent intent = new Intent(FibActivity.this, MainActivity.class);
                        startActivity(intent);
                    }
                }
        );
    }

}
