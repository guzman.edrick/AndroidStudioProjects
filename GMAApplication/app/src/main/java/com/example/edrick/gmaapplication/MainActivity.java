package com.example.edrick.gmaapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.edrick.FibActivity;
import com.example.edrick.Palindrome;
import com.example.edrick.Reasons;

public class MainActivity extends AppCompatActivity {
    private static Button fibAndFactorial;
    private static Button palindrome;
    private static Button reasons;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        OnClickButtonListener();
    }

    public void OnClickButtonListener(){
        fibAndFactorial = (Button)findViewById(R.id.fibAndFactorial);
        fibAndFactorial.setOnClickListener(
                new View.OnClickListener(){
                    @Override
                    public void onClick(View v){
                        Intent intent = new Intent(MainActivity.this, FibActivity.class);
                        startActivity(intent);
                    }
                }
        );

        palindrome = (Button)findViewById(R.id.palindrome);
        palindrome.setOnClickListener(
                new View.OnClickListener(){
                    @Override
                    public void onClick(View v){
                        Intent intent = new Intent(MainActivity.this, Palindrome.class);
                        startActivity(intent);
                    }
                }
        );

        reasons = (Button)findViewById(R.id.reasons);
        reasons.setOnClickListener(
                new View.OnClickListener(){
                    @Override
                    public void onClick(View v){
                        Intent intent = new Intent(MainActivity.this, Reasons.class);
                        startActivity(intent);
                    }
                }
        );
    }




}
