package com.example.model;

/**
 * Created by Edrick on 5/24/2017.
 */
public class palindromeActivityMethods {

    public static String checkIfPalindrome(String input)
    {
        char[] array = input.toCharArray();
        int i= 0;
        int j = array.length-1;
        int middle = (array.length-1)  /2;
        if(array.length % 2 != 0)
        {
            while (i != middle && j != middle) {
                if (array[i] != array[j]) {
                    return "No";
                }
                i++;
                j--;
            }

            return "Yes";

        }


        else
        {
            while (i != middle-1 && j != middle+1) {
                if (array[i] != array[j]) {
                    return "No";
                }
                i++;
                j--;
            }

            return "Yes";
        }


    }
}
