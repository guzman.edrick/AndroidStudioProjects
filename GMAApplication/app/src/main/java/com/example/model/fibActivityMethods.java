package com.example.model;

/**
 * Created by Edrick on 5/24/2017.
 */
public class fibActivityMethods {

    public static int fibonacci(int input)
    {
        int start = 1;
        int end = 1;

        for(int i = 2; i <= input-1;i++)
        {
            int sum = start + end;
            start = end;
            end = sum;
        }
        return end;
    }

    public static int factorial(int input)
    {
        int answer =1;

        for(int i = 1; i<= input; i++)
        {
            answer = i * answer;
        }

        return answer;
    }
}
