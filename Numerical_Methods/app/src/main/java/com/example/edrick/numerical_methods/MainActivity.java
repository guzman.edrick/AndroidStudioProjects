package com.example.edrick.numerical_methods;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import model.mainVariableModel;
import model.taylorConversionModel;
public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onButtonClick(View v)
    {

        mainVariableModel mv = new mainVariableModel();
        taylorConversionModel tcm = new taylorConversionModel();

        EditText numeratorEditText = (EditText)findViewById(R.id.numeratorField);
        EditText denominatorEditText = (EditText)findViewById(R.id.denominatorField);
        TextView newPoly = (TextView)findViewById(R.id.newPolynomialLbl);
        TextView answer = (TextView)findViewById(R.id.answerLabel);
        int limit = 1;
        String numerator = numeratorEditText.getText().toString();
        String denominator = denominatorEditText.getText().toString();
        mv.setLimit(1);
        mv.setNumerator(numerator);
        mv.setDenominator(denominator);

        mv.setPolynomial("(" + mv.getNumerator() + ")/" + mv.getDenominator());
        mv.setHighestDegree(tcm.findHighestOrder(mv.getPolynomial()));
        mv.setFirstStep(tcm.findWhatToReplace(mv.getNumerator()));
        mv.setSecondStep(tcm.getParameter(mv.getFirstStep()));
        mv.setThirdStep(tcm.replaceToReplace(mv.getSecondStep(),mv.getHighestDegree(),mv.getFirstStep()));
        mv.setFourthStep(mv.getNumerator().replace(mv.getFirstStep(), "(" + mv.getThirdStep() + ")"));
        mv.setFifthStep(tcm.evaluateFactorial(mv.getFourthStep())+"/"+mv.getDenominator());
//        mv.setSixthStep(tcm.distributeOperator(mv.getFifthStep()));
        mv.setAnswer(tcm.finalEvaluation(mv.getSixthStep(), 1));


        newPoly.setText(mv.getFifthStep() + " as limit approaches to " + 1);
        answer.setText(Double.toString(mv.getAnswer()));




    }
}
