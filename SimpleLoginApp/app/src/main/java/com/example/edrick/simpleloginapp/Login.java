package com.example.edrick.simpleloginapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class Login extends AppCompatActivity {

    private static EditText username;
    private static EditText password;
    private static TextView attempts;
    private static Button butt;

    int attempt_counter = 5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        LoginButton();

        attempts.setText(Integer.toString(attempt_counter));

    }

    public void LoginButton(){
        username = (EditText)findViewById(R.id.editText_username);
        password = (EditText)findViewById(R.id.editText_password);
        attempts = (TextView)findViewById(R.id.textView_attempt);
        butt = (Button)findViewById(R.id.button);

        butt.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(username.getText().toString().equals("user") &&
                                password.getText().toString().equals("pass")){
                            Toast.makeText(Login.this, "Username and password is correct",
                                    Toast.LENGTH_SHORT).show();
                            Intent i = new Intent(Login.this, user.class);
                            startActivity(i);
                        }
                        else{
                            Toast.makeText(Login.this, "Username or password is incorrect",Toast.LENGTH_SHORT).show();
                            attempt_counter--;
                            attempts.setText(Integer.toString(attempt_counter));
                            if(attempt_counter == 0){
                                butt.setEnabled(false);
                            }
                        }
                    }
                }
        );

    }

    public void anotherButton(View view){

        if(view.getId() == R.id.listButton){
            Intent i = new Intent(Login.this, listview.class);
            startActivity(i);

        }
    }


}
