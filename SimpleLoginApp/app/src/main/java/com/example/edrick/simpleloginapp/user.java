package com.example.edrick.simpleloginapp;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

/**
 * Created by Edrick on 5/15/2016.
 */
public class user extends Activity {

    private static ImageView img;
    private static Button butt;

    private int current_image_index;
    int[] images = {R.drawable.img1,R.drawable.img2,R.drawable.img3};

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);
        onButtonClick();
    }

    public void onButtonClick(){
        img = (ImageView)findViewById(R.id.image1);
        butt =(Button)findViewById(R.id.button2);
        butt.setOnClickListener(

                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        current_image_index++;
                        current_image_index= current_image_index % images.length;
                        img.setImageResource(images[current_image_index]);
                    }
                }
        );


    }
}
