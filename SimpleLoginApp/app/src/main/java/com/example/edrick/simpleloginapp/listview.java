package com.example.edrick.simpleloginapp;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

/**
 * Created by Edrick on 5/15/2016.
 */

public class listview extends Activity {

    private static ListView list_view;
    private String[] programmingLanguage = new String[] {"C++", "Java", "HTML", "CSS", "JavaScript", "SQL", "JQuery"};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        listView();
    }

    public void listView(){
        list_view = (ListView)findViewById(R.id.listView);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(listview.this, R.layout.activity_listview);
        list_view.setAdapter(adapter);
        list_view.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        String value = (String)list_view.getItemAtPosition(position);
                        Toast.makeText(listview.this, "Position:" + position + "Value:" + value, Toast.LENGTH_LONG).show();
                    }
                }

        );
    }

}
