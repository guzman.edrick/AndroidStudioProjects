package com.example.edrick.simlecalculator;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private static RadioGroup radio_g;
    private static RadioButton radio_add, radio_subtract, radio_multiply, radio_divide;
    private static Button calculate;
    private static Button exit_button;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        onButtonClickListener();
    }

    public void onButtonClickListener(){
        exit_button = (Button)findViewById(R.id.exit);
        exit_button.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v){
                        AlertDialog.Builder myBuilder = new AlertDialog.Builder(MainActivity.this); // nandito yung magpoprompt
                        myBuilder.setMessage("Exit???")
                                .setCancelable(false)
                                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        finish();
                                    }

                                }).setNegativeButton("No", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.cancel();
                                    }
                                }
                        );
                        AlertDialog alert = myBuilder.create(); //initialize yung alert
                        alert.setTitle("ALERT!!!");
                        alert.show();
                    }
                }
        );
    }
    public void onButtonClick(View v){
        EditText num1 = (EditText)findViewById(R.id.num1);
        EditText num2 = (EditText) findViewById(R.id.num2);
        TextView answer = (TextView) findViewById(R.id.textView);
        radio_g = (RadioGroup) findViewById(R.id.operations);
        calculate = (Button) findViewById(R.id.calc);
        RadioButton radio_add = (RadioButton) findViewById(R.id.radioButton_add);
        RadioButton radio_subtract = (RadioButton) findViewById(R.id.radioButton_subtract);
        RadioButton radio_multiply = (RadioButton) findViewById(R.id.radioButton_multiply);
        RadioButton radio_divide = (RadioButton) findViewById(R.id.radioButton_divide);


        int number1 = Integer.parseInt(num1.getText().toString());
        int number2 = Integer.parseInt(num2.getText().toString());
        int result = 0;
        if(radio_add.isChecked()){
            result = number1 + number2;
            answer.setText("Result:" + " " + Integer.toString(result));
        }
        else if(radio_subtract.isChecked()){
            result = number1 - number2;
            answer.setText("Result:" + " " + Integer.toString(result));
        }
        else if(radio_multiply.isChecked()){
            result = number1 * number2;
            answer.setText("Result:" + " " + Integer.toString(result));
        }
        else{
            result = number1/number2;
            answer.setText("Result:" + " " + Integer.toString(result));
        }





        //sum.setText("Result:" + " " + Integer.toString(result));



    }
}
