package com.example.edrick.practice;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;


/**
 * Created by Edrick on 5/14/2016.
 */
public class gallery extends Activity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gallery);
    }

    public void onClickButton(View v){
        if(v.getId() == R.id.button){
            Intent intent = new Intent(gallery.this, MainActivity.class);
            startActivity(intent);
        }

    }
}
