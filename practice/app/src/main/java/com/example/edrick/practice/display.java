package com.example.edrick.practice;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

/**
 * Created by Edrick on 5/13/2016.
 */
public class display extends Activity{
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.display);
    }

    public void onClickButton(View v){
        if(v.getId() == R.id.button){
            Intent intent = new Intent(display.this, MainActivity.class);
            startActivity(intent);
        }

    }

}
