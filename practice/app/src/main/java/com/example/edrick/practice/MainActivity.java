package com.example.edrick.practice;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onButtonClick(View v){
        if(v.getId() == R.id.bDisplay){
            Intent i = new Intent(MainActivity.this, display.class);
            startActivity(i);
        }
        else if(v.getId() == R.id.bSkills){
            Intent i = new Intent(MainActivity.this, displayskills.class);
            startActivity(i);
        }
        else if(v.getId() == R.id.bGallery){
            Intent i = new Intent(MainActivity.this, gallery.class);
            startActivity(i);
        }
        else if(v.getId() == R.id.bOthers){
            Intent i = new Intent(MainActivity.this, otherskills.class);
            startActivity(i);
        }

    }




}
