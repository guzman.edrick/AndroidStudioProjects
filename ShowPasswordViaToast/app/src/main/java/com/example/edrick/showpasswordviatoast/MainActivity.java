package com.example.edrick.showpasswordviatoast;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity {

    private EditText pw;
    private Button butt;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        addListenerOnButton();
    }

    public void addListenerOnButton() {
        pw = (EditText) findViewById(R.id.password);//set variable for password field
        butt = (Button) findViewById(R.id.button);//set variable for button
        butt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(//nandito yung message
                        MainActivity.this, pw.getText() ,
                        Toast.LENGTH_SHORT
                ).show(); //shows the message for password field
            }
        });


    }


}
