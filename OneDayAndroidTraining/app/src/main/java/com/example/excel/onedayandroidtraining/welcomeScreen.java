package com.example.excel.onedayandroidtraining;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

/**
 * Created by EXCEL on 6/11/2016.
 */
public class welcomeScreen extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.samplenotify);
        Thread timer = new Thread(){

            public void run(){
                try{
                    sleep(1000);
                }
                catch(InterruptedException e){
                }
                finally{
                    Intent newWindow = new Intent("com.example.excel.onedayandroidtraining.sampleActivity");
                    startActivity(newWindow);

                }
            }
        };
        timer.start();
    }
}
