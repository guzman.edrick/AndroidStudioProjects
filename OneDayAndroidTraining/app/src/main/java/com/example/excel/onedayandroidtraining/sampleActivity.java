package com.example.excel.onedayandroidtraining;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by EXCEL on 6/11/2016.
 */
public class sampleActivity extends Activity implements AdapterView.OnItemSelectedListener {
    TextView tvName;
    EditText edName;
    Button btOk, btReset;
    CheckBox chkStatus;
    RadioButton rdMale, rdFemale;
    Spinner listColor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.samplelayout);
        init();
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.colors, android.R.layout.simple_spinner_dropdown_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        listColor.setAdapter(adapter);
    }

    public void init(){
        tvName = (TextView) findViewById(R.id.tvName);
        edName = (EditText) findViewById(R.id.edName);
        btOk = (Button) findViewById(R.id.btOk);
        btReset = (Button) findViewById(R.id.btReset);
        chkStatus = (CheckBox) findViewById(R.id.chkStatus);
        rdMale = (RadioButton) findViewById(R.id.rdMale);
        rdFemale = (RadioButton) findViewById(R.id.rdFemale);
        listColor = (Spinner) findViewById(R.id.listColor);
        listColor.setOnItemSelectedListener((AdapterView.OnItemSelectedListener)this);
    }

    public void checkGender(View view){
        if(rdMale.isChecked() == true){
            Toast.makeText(sampleActivity.this, "You are a male", Toast.LENGTH_SHORT).show();
        }
        else if(rdFemale.isChecked() == true) {
            Toast.makeText(sampleActivity.this, "You are a female", Toast.LENGTH_SHORT).show();
        }
    }

    public void greet(View view){
        String name;
        name = edName.getText().toString();
        Toast.makeText(sampleActivity.this, "Hello " + name, Toast.LENGTH_LONG).show();

    }

    public void reset(View view){
        edName.setText("");

    }

    public void checkStatus(View view){
        if(chkStatus.isChecked() == true){
            Toast.makeText(sampleActivity.this, "Checkbox is checked", Toast.LENGTH_LONG).show();
        }
        else{
            Toast.makeText(sampleActivity.this, "Checkbox is not checked", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) { //position =index
        if(position == 0){
            Toast.makeText(sampleActivity.this, "Blue", Toast.LENGTH_SHORT).show();
        }
        if(position == 1){
            Toast.makeText(sampleActivity.this, "Black", Toast.LENGTH_SHORT).show();
        }
        if(position == 2){
            Toast.makeText(sampleActivity.this, "Green", Toast.LENGTH_SHORT).show();
        }
        if(position == 3){
            Toast.makeText(sampleActivity.this, "Red", Toast.LENGTH_SHORT).show();
        }



    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
