package com.example.excel.onedayandroidtraining;

import android.app.ListActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

/**
 * Created by EXCEL on 6/11/2016.
 */
public class listActivity extends ListActivity {

    String[] artist = {"Michael Buble", "John Mayer", "Jason Mraz", "Josh Groban"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setListAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, artist));
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        if(position == 0){
            Toast.makeText(this, "Michael Buble", Toast.LENGTH_SHORT).show();
        }
        if(position == 1){
            Toast.makeText(this, "John Mayer", Toast.LENGTH_SHORT).show();
        }
        if(position == 2){
            Toast.makeText(this, "Jazon Mraz", Toast.LENGTH_SHORT).show();
        }
        if(position == 3){
            Toast.makeText(this, "Josh Groban", Toast.LENGTH_SHORT).show();
        }
    }
}
