package com.example.excel.onedayandroidtraining;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by EXCEL on 6/11/2016.
 */
public class dbConnect {


    public static final String DB_NAME = "myDatabase";
    public static final String TABLE_NAME= "myTable";
    public static final String NAME= "name";
    public static final String EMAIL = "email";
    public static final int DB_VERSION = 1;

    public static final String DB_CREATE = "CREATE TABLE myTable (name text not null, email text not null)";

    DatabaseHelper dbHelper;
    Context ctx;
    SQLiteDatabase db;

    public dbConnect(Context ctx){
        this.ctx = ctx;
        dbHelper = new DatabaseHelper(ctx);
    }

    private static class DatabaseHelper extends SQLiteOpenHelper{
        public DatabaseHelper(Context ctx){
            super(ctx, DB_NAME, null, DB_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(DB_CREATE);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL("DROP TABLE on Exists myTable");
            onCreate(db);

        }
    }

    public dbConnect open(){
        db = dbHelper.getWritableDatabase();
        return this;
    }
    public void close(){
        dbHelper.close();
    }

    public long insertData(String name, String email){

        ContentValues data = new ContentValues();
        data.put(NAME, name);
        data.put(EMAIL, email);

        return db.insertOrThrow(TABLE_NAME, null, data);
    }

    public Cursor loadData(){
        String sql = "SELECT * FROM myTable";
        return db.rawQuery(sql, null);


    }



    public void updateData(String name, String email){
        String sql = "UPDATE myTable SET email ='" + email + "'WHERE name = '" + name + "'";
        db.execSQL(sql);

    }

    public void deleteData(String name){
        String sql = "DELETE FROM myTable WHERE name='" + name + "'";
        db.execSQL(sql);

    }

}

