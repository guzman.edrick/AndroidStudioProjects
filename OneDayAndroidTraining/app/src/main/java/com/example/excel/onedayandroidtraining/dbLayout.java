package com.example.excel.onedayandroidtraining;

import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by EXCEL on 6/11/2016.
 */
public class dbLayout extends Activity{
    EditText edName,edName2, edMail, edMail2;
    Button btSave, btLoad, btNext, btPrev, btUpdate, btDelete;
    dbConnect handler;
    int position = 0;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.database);
        init();
        handler = new dbConnect(getBaseContext());

    }

    public void init(){

        edName = (EditText) findViewById(R.id.edName);
        edName2 = (EditText) findViewById(R.id.edName2);
        edMail = (EditText) findViewById(R.id.edMail);
        edMail2 = (EditText) findViewById(R.id.edMail2);
        btSave = (Button) findViewById(R.id.btSave);
        btLoad = (Button) findViewById(R.id.btLoad);
        btNext = (Button) findViewById(R.id.btNext);
        btPrev = (Button) findViewById(R.id.btPrev);
        btUpdate = (Button) findViewById(R.id.btUpdate);
        btDelete = (Button) findViewById(R.id.btDelete);

    }

    public void save(View view){
        handler.open();
        String name, mail;
        name = edName.getText().toString();
        mail = edMail.getText().toString();

        handler.insertData(name, mail);
        Toast.makeText(this,"Record inserted", Toast.LENGTH_SHORT).show();

        handler.close();
    }
    public void load(View view){
        handler.open();
        String name, mail;
        name = "";
        mail = "";
        Cursor c = handler.loadData();

        c.moveToPosition(position);

        name = c.getString(0);
        mail = c.getString(1);
        edName2.setText(name);
        edMail2.setText(mail);


        handler.close();
    }

    public void next(View view){
        handler.open();
        String name, mail;
        name = "";
        mail = "";

        Cursor c = handler.loadData();
        if(position == c.getCount()-1){
            Toast.makeText(this, "End of File", Toast.LENGTH_SHORT).show();
        }
        else{
            position++;
            c.moveToPosition(position);

            name = c.getString(0);
            mail = c.getString(1);
            edName2.setText(name);
            edMail2.setText(mail);


            handler.close();
        }


        handler.close();


    }

    public void prev(View view){
        handler.open();
        String name, mail;
        name = "";
        mail = "";

        Cursor c = handler.loadData();
        if(position == 0){
            
            Toast.makeText(this, "End of File", Toast.LENGTH_SHORT).show();
        }
        else{
            position--;
            c.moveToPosition(position);

            name = c.getString(0);
            mail = c.getString(1);
            edName2.setText(name);
            edMail2.setText(mail);


            handler.close();
        }


        handler.close();
    }



    public void update(View view){
        handler.open();
        String name = edName2.getText().toString();
        String mail = edMail2.getText().toString();

        handler.updateData(name, mail);
        Toast.makeText(dbLayout.this, "Record Updated", Toast.LENGTH_SHORT).show();
        handler.close();
    }

    public void delete(View view){
        handler.open();
        String name = edName2.getText().toString();


        handler.deleteData(name);
        Toast.makeText(dbLayout.this, "Record Deleted", Toast.LENGTH_SHORT).show();
        handler.close();

    }




}
