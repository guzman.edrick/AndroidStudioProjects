package com.example.edrick.ratingbar;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private static Button butt;
    private static RatingBar ratingBar;
    private static TextView ratingView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listenerForRatingBar();
        onButtonClickListener();
    }

    public void listenerForRatingBar(){
        ratingBar = (RatingBar)findViewById(R.id.ratingBar);
        ratingView = (TextView)findViewById(R.id.ratingView);

        ratingBar.setOnRatingBarChangeListener(
                new RatingBar.OnRatingBarChangeListener() {
                    @Override
                    public void onRatingChanged(RatingBar rateBar, float rating, boolean f){
                        ratingView.setText(String.valueOf(rating));
                    }
                }
        );

    }

    public void onButtonClickListener(){
        ratingBar = (RatingBar)findViewById(R.id.ratingBar);
        butt = (Button)findViewById(R.id.button);

        butt.setOnClickListener(
                new View.OnClickListener(){
                    @Override
                    public void onClick(View v){
                        Toast.makeText
                                (MainActivity.this,
                                String.valueOf(ratingBar.getRating()),
                                Toast.LENGTH_SHORT).show();

                    }
                }
        );
    }
}
